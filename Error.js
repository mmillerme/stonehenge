var Error = function (properties) {
	for (var property in properties) {
		this[property] = properties[property];
	}
};

Error.prototype.toString = function () {
	return JSON.stringify(this);
};

Error.respond = function (res, error) {
	if (error instanceof Error) {
		res.status(error.status).json(error);
	} else {
		console.error(error);
	}
};

module.exports = Error;