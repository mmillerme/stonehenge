# README #

Stonehenge is a website content management system developed using Node.js + Express.js and Nunjucks for templating; and jQuery + RequireJS + RedactorJS on the frontend.

## Copyright ##

Stonehenge is copyright 2015 [Matt Miller](http://mmiller.me/). The logo was designed by [Freepik](http://www.freepik.com/) from [Flaticon](http://www.flaticon.com/) and licensed by [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/).