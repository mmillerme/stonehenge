require('amd-loader');

var stonehenge = {};
stonehenge.express = require('express');
stonehenge.body_parser = require('body-parser');
stonehenge.url_decoder = stonehenge.body_parser.urlencoded({ extended: false });

stonehenge.express()
	.set('strict routing', true)
	.use('/\\$(/\\$)?', require('./routes/api')(stonehenge))
	.use('*', require('./routes/renderer')(stonehenge))
	.listen(4242, function () {
		console.log('Stonehenge initializing... reticulating splines');
	});