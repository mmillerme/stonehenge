var fs = require('fs');
module.exports = function () {
	return JSON.parse(fs.readFileSync(__dirname + '/content/config.json', 'utf8'));
};