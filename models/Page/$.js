define(function (require, exports, module) {
	var Page = function (slug, done) {
		var instance = this;

		instance._construct(slug, done);
	};

	Page.prototype = {
		_construct: function (slug, done) {
			var instance = this;

			instance._is_new = true;

			if (typeof slug == 'string') {
				instance.read(slug, function (res, err) {
					instance._is_new = false;
					!err ? done.call(instance) : done.call(null, null, err);
				});
			}
		},

		validateSlug: function (slug) {
			var instance = this;

			if (true) { // TODO: slug validation
				return true;
			} else {
				throw new Page._Error({
					message: 'The provided slug is invalid.',
					status: 400,
					slug: slug
				});
			}
		},

		validateProperties: function (properties) {
			var instance = this;

			instance._is_new; // TODO: validate properties
			if (true) {
				return true;
			} else {
				throw new Page._Error({
					message: 'The property is invalid.',
					status: 400,
					properties: properties
				});
			}
		},

		slugExists: function (slug, done) {
			var instance = this;

			instance.validateSlug(slug);
			instance._slugExists(slug, done);
		},

		read: function (slug, done) {
			var instance = this;

			instance.validateSlug(slug);
			instance.slugExists(slug, function (exists) {
				if (exists) {
					instance._read(slug, function (properties) {
						instance.slug = slug;
						instance.properties = properties;
						done.call(instance);
					});
				} else {
					done.call(instance, null, new Page._Error({
						message: 'The requested page does not exist.',
						status: 404,
						slug: slug
					}));
				}
			});
		},

		save: function (done) {
			var instance = this;

			instance.validateProperties(instance.properties);
			instance.slugExists(instance.slug, function (exists) {
				if (exists) {
					instance._create(function (err) {
						i
						instance._is_new = false;
						done.call(instance);
					});
				} else {
					instance._update(done);
				}
			});
		},

		'delete': function (done) {
			var instance = this;

			instance.slugExists(instance.slug, function (exists) {
				if (exists) {
					instance._delete(function () {
						instance._is_new = true;
						delete instance.slug;
						delete instance.properties;
						done.call(instance);
					});
				} else {
					done.call(instance, null, new Page._Error({
						message: 'The requested page does not exist.',
						status: 404,
						slug: instance.slug
					}));
				}
			});
		},

		_checkRender: function () {
			var instance = this;

			if (instance.properties && instance.properties.view && instance.properties.regions) {
				return true;
			} else {
				done.call(new Page._Error({
					message: 'The view and/or regions are not defined.',
					status: 400,
					slug: instance.slug
				}));
			}
		},

		render: function (done, view_source, region_filter) {
			var instance = this;

			var ready = function (regions) {
				instance._checkRender();
				done.call(
					instance,
					!view_source
						? Page._nunjucks.render(instance.properties.view + '.html', regions)
						: Page._nunjucks.renderString(view_source, regions)
				);
			};

			var regions = JSON.parse(JSON.stringify(instance.properties.regions));
			if (!region_filter) {
				ready(regions);
			} else {
				var finished = 0;
				for (var region in regions) {
					region_filter(region, regions[region], function (output) {
						regions[region] = output;
						if ((++finished) == Object.keys(regions).length) {
							ready(regions);
						}
					});
				}
			}
		}
	};

	return Page;
});