define(
	[ 'config', 'Error', 'abstracts/Page', 'lib/regions', 'lib/toolbar', 'jquery', 'nunjucks' ],
	function (config, Error, Page, regions, toolbar, $, nunjucks) {
		Page._pages = {};
		Page._nunjucks = nunjucks;
		Page._Error = Error;

		Page.prototype._slugExists = function (slug, done) {
			var instance = this;

			if (Page._pages[slug]) {
				done.call(instance, true);
			} else {
				$.get('/$/pages/' + slug)
					.done(function (properties) {
						Page._pages[slug] = properties;
						done.call(instance, true);
					})
					.fail(function () {
						done.call(instance, false);
					});
			}
		};

		Page.prototype._create = function (done) {
			// TODO: implement create
			var instance = this;
			$.ajax('/$/pages/' + instance.slug, {
				type: 'PUT',
				data: {
					slug: instance.slug,
					properties: instance.properties
				}
			})
				.done(function () {
					done.call(instance);
				})
				.fail(function () {
					doen.call(instance, new Error({
						status: jqXHR.status,
						message: jqXHR.responseJSON
					}));
				});
		};

		Page.prototype._update = function (done) {
			// TODO: implement update
		};

		Page.prototype._read = function (slug, done) {
			var instance = this;

			if (Page._pages[slug]) {
				done.call(instance, Page._pages[slug]);
			} else {
				$.get('/$/pages/' + slug)
					.done(function (properties) {
						Page._pages[slug] = properties;
						instance.properties = properties;
						done.call(instance, properties);
					})
					.fail(function (jqXHR) {
						done.call(instance, null, new Error({
							status: jqXHR.status,
							message: jqXHR.responseJSON
						}));
					});
			}
		};

		Page.prototype._delete = function (done) {
			// TODO: implement delete
		};

		Page.prototype.renderAdmin = function () {
			var instance = this;

			$('body').attr('data-stonehenge', true);

			regions(instance);
			toolbar.initialize(instance);
		};

		return Page;
	}
);