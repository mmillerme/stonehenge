var config = require('../../config')();
var Error = require('../../Error');
var Scheme = require('../Scheme/server');
var escape = require('escape-html');

var nunjucks = require('nunjucks');
nunjucks.configure('./content/schemes/' + config.scheme + '/views');

var PAGES_ROOT = 'content/pages';

var Page = require('./$');

Page._pages = {};
Page._nunjucks = nunjucks;
Page._Error = Error;

Page.prototype._slugToPath = function () {
	var instance = this;

	if (!instance.slug) {
		throw new Error({
			message: '_slugToPath requires a slug',
			status: 400
		});
	}

	return (
		'./' + PAGES_ROOT + '/' +
		(instance.slug != '/' ? instance.slug : '') +
		(instance.slug.substr(-1) == '/' ? '$' : '') +
		'.json'
	);
};

Page.prototype._slugExists = function (slug, done) {
	var instance = this;

	done.call(instance, !!Page._pages[slug]);
};

Page.prototype._create = function (done) {
	// TODO: implement create
};

Page.prototype._update = function (done) {
	// TODO: implement update
};

Page.prototype._read = function (slug, done) {
	var instance = this;

	done.call(instance, Page._pages[slug]);
};

Page.prototype._delete = function (done) {
	// TODO: implement delete
};

Page.prototype.renderAdmin = function (done) {
	var instance = this;

	new Scheme(config.scheme, function (err) {
		if (!err) {
			this.getView(instance.properties.view, function (source, properties, err) {
				if (!err) {
					var regions = properties.regions;
					var hidden_regions = {};

					instance.render(
						function (html) {
							var script = '<script src="/$/stonehenge/lib/vendor/require-2.1.15.js" data-main="/$/stonehenge/app"></script>' +
									'<script>window.stonehenge_hidden_regions = ' + JSON.stringify(hidden_regions) + ';</script>';
							var body_closing_index = html.toLowerCase().lastIndexOf('</body>');
							if (body_closing_index > -1) {
								html = html.substr(0, body_closing_index) +
								script +
								html.substr(body_closing_index);
							} else {
								html += script;
							}

							done.call(instance, html);
						},
						undefined,
						function (region_id, source, done) {
							var region = regions[region_id];
							if (region) {
								if (!region.hidden) {
									var tag = region.multiline ? 'div' : 'span';
									done(
										'<' + tag + ' data-stonehenge-region="' +
										escape(region_id) + '" data-stonehenge-region-flags="' +
										escape(JSON.stringify(region)) + '">' + source + '</' + tag + '>'
									);
								} else {
									region.hidden && (hidden_regions[region_id] = region);
									done(source);
								}
							} else {
								done('');
							}
						}
					);
				} else {
					console.error(err);
					done.call(null, null, err);
				}
			});
		} else {
			console.error(err);
			done.call(null, null, err);
		}
	});
};

Page.getPages = function (done) {
	done.call(Page, Object.keys(Page._pages));
};

require('recursive-readdir')('./' + PAGES_ROOT, function (err, files) {
	if (!err) {
		var fs = require('fs');

		var read = 0;
		var pathToSlug = function (path) {
			return (
				path
					.substr((PAGES_ROOT + '/').length)
					.toLowerCase()
					.replace(/\\/g, '/')
					.slice(0, -'.json'.length)
					.split('$')
					[0] ||
				'/'
			);
		};

		files.forEach(function (file) {
			var slug = pathToSlug(file);
			fs.readFile(file, function (err, contents) {
				if (!err) {
					try {
						Page._pages[slug] = JSON.parse(contents);
					} catch (err) {
						console.error('Error parsing page ' + file + ': ' + err);
					}
				} else {
					console.error(err);
				}

				++read;
				if (read == files.length) {
					console.log('All pages loaded.');
				}
			});
		});
	} else {
		console.error(err);
	}
});

module.exports = Page;