define(function (require, exports, module) {
	var Scheme = function (name, done) {
		var instance = this;

		instance._construct(name, done);
	};

	Scheme.prototype = {
		_construct: function (name, done) {
			var instance = this;

			if (typeof name == 'string') {
				instance.read(name, function (properties, err) {
					!err ? done.call(instance) : done.call(null, err);
				});
			}
		},

		read: function (name, done) {
			var instance = this;

			instance._read(name, function (properties, err) {
				if (!err) {
					instance.name = name;
					instance.properties = properties;
					done.call(instance);
				} else {
					done.call(null, null, err);
				}
			});
		},

		getView: function (view, done) {
			var instance = this;

			if (instance.properties && instance.properties.views) {
				var properties = instance.properties.views[view];
				if (properties) {
					instance.getViewSource(view, function (source, err) {
						if (!err) {
							done.call(instance, source, properties);
						} else {
							done.call(null, null, null, new Scheme._Error({
								message: 'Could not get source of view ' + view + ' in scheme ' + config.scheme,
								status: 404,
								name: instance.name,
								view: view
							}));
						}
					});
				} else {
					done.call(null, null, null, new Scheme._Error({
						message: 'The given view does not exist in the scheme',
						status: 404,
						name: instance.name,
						view: view
					}));
				}
			} else {
				done.call(null, null, null, new Scheme._Error({
					message: 'The views are not defined',
					status: 400,
					name: instance.name
				}));
			}
		}
	};

	return Scheme;
});