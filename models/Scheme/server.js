var fs = require('fs');
var Error = require('../../Error');
var SCHEMES_ROOT = 'content/schemes';
var Scheme = require('./$');
var config = require('../../config')();

Scheme.prototype._read = function (name, done) {
	var instance = this;

	if (Scheme._schemes[name]) {
		done.call(instance, Scheme._schemes[name]);
	} else {
		done.call(null, null, new Error({
			message: 'The scheme does not exist',
			status: 404,
			scheme: name
		}));
	}
};

Scheme.prototype.getViewSource = function (view, done) {
	var instance = this;

	fs.readFile('./' + SCHEMES_ROOT + '/' + config.scheme + '/views/' + view + '.html', 'utf8',
		function (err, source) {
			if (!err) {
				done.call(instance, source);
			} else {
				done.call(null, null, err);
			}
		}
	);
};

Scheme._schemes = {};
Scheme._Error = Error;

Scheme.getSchemes = function (done) {
	done.call(Scheme, Object.keys(Scheme._schemes));
};

fs.readdir('./' + SCHEMES_ROOT, function (err, dirs) {
	if (!err) {
		var read = 0;

		dirs.forEach(function (dir) {
			fs.readFile('./' + SCHEMES_ROOT + '/' + dir + '/' + dir + '.json', function (err, contents) {
				if (!err) {
					try {
						Scheme._schemes[dir] = JSON.parse(contents);
					} catch (err) {
						console.error('Error parsing scheme ' + dir + ': ' + err);
					}
				} else {
					console.error('Error loading scheme ' + dir+ ': ' + err);
				}

				++read;
				if (read == dirs.length) {
					console.log('All schemes loaded.');
				}
			});
		});
	} else {
		console.error(err);
	}
});

module.exports = Scheme;