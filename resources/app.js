require.config({
	baseUrl: '/$/stonehenge/',
	paths: {
		css: 'lib/vendor/css',

		jquery: 'lib/vendor/jquery-2.1.3',
		redactor: 'lib/vendor/jquery.redactor-10.0.7/redactor',
		nunjucks: 'lib/vendor/nunjucks-1.1.0',
		ace: 'lib/vendor/ace-1.1.8',

		Error: 'lib/Error',
		config: 'config?',

		'private-jquery': 'lib/private-jquery',

		'models/Page': 'models/Page?',
		'abstracts/Page': 'abstracts/Page?'
	},
	map: {
		'*': {
			jquery: 'private-jquery!'
		},
		'private-jquery': { jquery: 'jquery' }
	}
});

require(
	[ 'models/Page', 'css!app.css' ],
	function (Page) {
		new Page(window.location.pathname.split('!')[0], function (res, err) {
			if (!err) {
				this.renderAdmin();
			} else {
				alert(err);
			}
		});
	}
);