define(function () {
	var Error = function (properties) {
		for (var property in properties) {
			this[property] = properties[property];
		}
	};

	Error.prototype.toString = function () {
		return this.message;
	};

	return Error;
});