define([ 'jquery' ], function (jQuery) {
	return {
		load: function (resource, require, load) {
			require([ 'redactor', 'css!redactor.css', 'css!lib/vendor/fontawesome-4.3.0/css/font-awesome.css' ], function () {
				load(jQuery.noConflict(true));
			});
		}
	};
});