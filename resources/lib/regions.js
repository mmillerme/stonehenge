define([ 'jquery', 'ace/ace' ], function ($, ace) {
	return function (page, $context, no_preview) {
		var editRegion = function (region_id) {
			var $region = $('[data-stonehenge-region]', $context).filter(function () {
				return $(this).attr('data-stonehenge-region') == region_id;
			});

			if (!$region.length) {
				return;
			}

			var $edit_button = $('.stonehenge_edit', $region).detach();
			var dims = [ $region.width(), $region.height() ];
			var region = JSON.parse($region.attr('data-stonehenge-region-flags'));

			region.escape && (region.wysiwyg = false); // escape overrides wysiwyg
			var $input = region.multiline || region.wysiwyg
				? $('<textarea></textarea>')
				: $('<input>').attr('type', 'text');
			$region.empty();
			$input.prependTo($region);
			$input.val(page.properties.regions[region_id] || '');
			$input.css({ 'max-width': dims[0] + 'px', width: '100%' });
			(region.multiline || region.wysiwyg) && $input.height(dims[1]);

			if (!region.escape && region.multiline) {
				var ace_instance;
				var createAce = function () {
					ace_instance = ace.edit($input.get(0));
					ace_instance.setTheme('ace/theme/chrome');
					var s = ace_instance.getSession();
					s.setMode('ace/mode/html');
					s.setUseSoftTabs(false);
					s.setUseWorker(false);
					ace_instance.setShowPrintMargin(false);
					ace_instance.setOption('scrollPastEnd', false);
				};

				if (region.wysiwyg) {
					$input.redactor({
						buttons: [ 'html', 'formatting', 'bold', 'italic', 'unorderedlist', 'orderedlist', 'outdent',
							'indent', 'image', 'file', 'link', 'alignment', 'horizontalrule' ],
						focus: true
					});

					if (!no_preview) {
						var button = $input.redactor('button.addFirst', 'update', 'Update');
						$input.redactor('button.setAwesome', 'update', 'fa-check');
						$input.redactor('button.addCallback', button, function () {
							var content = $input.redactor('code.get');
							$input.redactor('core.destroy');
							$region.html(content);
							$edit_button.prependTo($region).css('display', '');
							page.properties.regions[region_id] = content;
							page.changed = true;
						});
					} else {
						$input.redactor('blurCallback', function () {
							page.properties.regions[region_id] = this.code.get();
							page.changed = true;
						});
					}

					$('.redactor-toolbar .re-html', $region).click(function () {
						if ($(this).is('.redactor-act')) {
							createAce();
						} else {
							var content = ace_instance.getValue();
							ace_instance.destroy();
							$('.ace_editor', $region).replaceWith($input.val(content));
							$input.redactor('code.set', content);
						}
					});
				} else if (region.multiline) {
					$region.attr('data-stonehenge-ace', 'true').wrapInner($('<div></div>'));
					createAce();
					if (!no_preview) {
						$('<a></a>')
							.addClass('stonehenge_button stonehenge_update fa fa-check')
							.attr('href', 'javascript:;')
							.appendTo($('> div', $region))
							.click(function () {
								var content = ace_instance.getValue();
								ace_instance.destroy();
								$region.html(content);
								$edit_button.prependTo($region).css('display', '');
								page.properties.regions[region_id] = content;
								page.changed = true;
							});
					} else {
						ace_instance.on('blur', function () {
							page.properties.regions[region_id] = ace_instance.getValue();
							page.changed = true;
						});
					}
				}
			} else {
				$input.detach();
				$region.empty();
				$input.appendTo($region);

				if (!no_preview) {
					$('<a></a>')
						.addClass('stonehenge_button stonehenge_update fa fa-check')
						.attr('href', 'javascript:;')
						.appendTo($region)
						.click(function () {
							var content = $input.val();
							$region.html(content);
							$edit_button.prependTo($region).css('display', '');
							page.properties.regions[region_id] = content;
							page.changed = true;
						});
				} else {
					$input.blur(function () {
						page.properties.regions[region_id] = $(this).val();
						page.changed = true;
					});
				}
			}
		};

		if (!no_preview) {
			!$('[data-stonehenge-region]', $context).each(function () {
				$('<a></a>')
					.addClass('stonehenge_button stonehenge_edit fa fa-pencil')
					.attr('href', 'javascript:;')
					.prependTo(this);
			}
			);

			$('body')
				.off('click.stonehenge.region')
				.on('click.stonehenge.region', '[data-stonehenge-region] .stonehenge_edit', function () {
					editRegion($(this).closest('[data-stonehenge-region]').attr('data-stonehenge-region'));
				}
			);
		} else {
			$('[data-stonehenge-region]', $context).each(function () {
				editRegion($(this).attr('data-stonehenge-region'));
			});
		}
	};
});