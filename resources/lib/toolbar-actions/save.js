define([ 'config', 'jquery', 'nunjucks' ], function (config, $, nunjucks) {
	var nj = nunjucks.configure('/$/stonehenge/views/renderer');

	return function (page) {
		page.save();
	};
});