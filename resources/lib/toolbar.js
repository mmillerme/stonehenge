define([ 'jquery' ], function ($) {
	return {
		initialize: function (page) {
			$.get('/$/stonehenge/views/renderer/toolbar.html', function (toolbar) {
				$('body').append(toolbar);

				var $window = $('#stonehenge_window');
				var $window_inner = $('#stonehenge_window_inner', $window);
				var $toolbar = $('#stonehenge_toolbar');


				$toolbar.on('click', 'a', function () {
					var $a = $(this);
					var $actions = $('li a', $toolbar);
					var window_open = $window.is('.open');

					var deferred = new $.Deferred();
					if (window_open && $a.is('[data-view]')) {
						$window
							.removeClass('open')
							.stop(true)
							.addClass('animating')
							.animate({ top: '100%' }, 500, deferred.resolve);
						$('body').removeClass('stonehenge_blur');
					} else {
						deferred.resolve();
					}

					deferred.done(function () {
						var view = $a.attr('data-view');
						var action = $a.attr('data-action');

						if (view) {
							if ($a.is('.active')) {
								$window.removeClass('animating');
								$a.removeClass('active');
								return;
							}
							$actions.removeClass('active');
							$a.addClass('active');
							$window_inner.empty();
							$window
								.addClass('open animating')
								.stop(true)
								.animate({ top: 0 }, 500, function () {
									$window.removeClass('animating');
									$('body').addClass('stonehenge_blur');
								});

							require([ 'lib/windows/' + view ], function (view) {
								view.render(page).done(function (html, done) {
									$window_inner.html(html);
									done.call($window_inner);
								});
							});
						} else if (action) {
							require([ 'lib/toolbar-actions/' + action ], function (module) {
								module(page);
							});
						}
					});
				});
			});
		}
	};
});