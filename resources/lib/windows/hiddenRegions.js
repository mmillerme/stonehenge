define([ 'jquery', 'nunjucks', 'lib/regions' ], function ($, nunjucks, regions) {
	var nj = nunjucks.configure('/$/stonehenge/views/renderer');

	return {
		render: function (page) {
			var deferred = new $.Deferred();

			var hidden_regions = window.stonehenge_hidden_regions;
			for (var id in hidden_regions) {
				hidden_regions[id]._json = JSON.stringify(hidden_regions[id]);
			}

			nj.render('hiddenRegions.html', { regions: hidden_regions }, function (err, html) {
				if (!err) {
					deferred.resolve(html, function () {
						var $container = this;
						regions(page, $container, true);
					});
				} else {
					throw err;
				}
			});

			return deferred.promise();
		}
	};
});