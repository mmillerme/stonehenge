define([ 'config', 'jquery', 'nunjucks', 'models/Page' ], function (config, $, nunjucks, Page) {
	var nj = nunjucks.configure('/$/stonehenge/views/renderer');

	return {
		render: function (page) {
			var deferred = new $.Deferred();
			$.when($.get('/$/pages'), $.get('/$/schemes/' + config.scheme + '/views')).done(function (pages, views) {
				pages = pages[0];
				views = views[0];

				pages.sort(function (a, b) {
					return a < b ? -1 : 1;
				});
				
				nj.render('pageManager.html', {
					pages: pages,
					page: page,
					views: views,
					is_slug_parent: function (slug) {
						return slug.length && slug.substring(slug.length - 1) == '/';
					}
				}, function (err, html) {
					if (!err) {
						deferred.resolve(html, function () {
							var $container = this;
							var $configure_page = $('#stonehenge_window_pageManager_configurePage', $container);

							$('ul', $container)
								.on('click', '.actions .create', function () {
									$configure_page.addClass('create').fadeIn(500);
									$(':input:first', $configure_page).focus();
								})
								.on('click', '.actions .configure', function () {
									$configure_page.addClass('edit').fadeIn(500);
									new Page($(this).closest('[data-slug]').attr('data-slug'), function () {
										$('#stonehenge_window_pageManager_configurePage_slug', $configure_page).val(this.slug).get(0).select();
										$('#stonehenge_window_pageManager_configurePage_view', $configure_page).val(this.properties.view);
									});
								})

							$configure_page
								.on('click', '.submit', function () {

								})
								.on('click', '.cancel', function () {
									$('input', $configure_page).val('');
									$('select', $configure_page).prop('selectedIndex', 0);
									$configure_page.fadeOut(250, function () { $configure_page.removeClass('create edit'); });
								});
						});
					} else {
						throw err;
					}
				});
			});

			return deferred.promise();
		}
	};
});