module.exports = function (stonehenge) {
	return stonehenge.express.Router({ strict: true })
		.use('/pages', require('./api/pages')(stonehenge))
		.use('/schemes', require('./api/schemes')(stonehenge))
		.use('/stonehenge', require('./api/stonehenge')(stonehenge))
		.all('/*', function (req, res) {
			res.status(400).json('Unknown API call');
		});
};