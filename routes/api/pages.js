var Error = require('../../Error');
var Page = require('../../models/Page/server');

module.exports = function (stonehenge) {
	return stonehenge.express.Router({ strict: true })
		.get('/', function (req, res, next) {
			if (req.originalUrl.substr(req.baseUrl.length) != '//') {
				Page.getPages(function (pages) {
					res.json(pages);
				});
			} else {
				next();
			}
		})
		.use('/*', function (req, res, next) {
			req.stonehenge = req.stonehenge || {};
			var slug = req.originalUrl.substr('/$/pages/'.length);
			req.stonehenge.page = new Page(slug, function (err) {
				if (!err) {
					setTimeout(next, 0);
				} else {
					Error.respond(res, err);
				}
			});
		})
		.get('/*', function (req, res) {
			res.json(req.stonehenge.page.properties);
		});
};