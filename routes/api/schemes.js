var Error = require('../../Error');
var Scheme = require('../../models/Scheme/server');

module.exports = function (stonehenge) {
	return stonehenge.express.Router()
		.get('/', function (req, res) {
			Scheme.getSchemes(function (schemes) {
				res.json(schemes);
			});
		})
		.use('/:scheme', function (req, res, next) {
			req.stonehenge = req.stonehenge || {};
			req.stonehenge.scheme = new Scheme(req.params.scheme, function (err) {
				if (!err) {
					setTimeout(next, 0);
				} else {
					Error.respond(res, err);
				}
			});
		})
		.use('/:scheme', stonehenge.express.Router()
			.get('', function (req, res) {
				res.json(req.stonehenge.scheme.properties);
			})
			.use('/views', stonehenge.express.Router()
				.get('/', function (req, res) {
					res.json(req.stonehenge.scheme.properties.views);
				})
				.use('/:view', stonehenge.express.Router({ mergeParams: true })
					.get('', function (req, res) {
						var view = req.params.view;
						var offset = Math.max(0, req.params.view.length - '.html'.length);
						if (req.params.view.substring(offset) == '.html') {
							view = req.params.view.substring(0, offset);
							req.stonehenge.scheme.getViewSource(view, function (source, err) {
								if (!err) {
									res.send(source);
								} else {
									Error.respond(res, err);
								}
							});
						} else {
							req.stonehenge.scheme.getView(view, function (source, regions, err) {
								if (!err) {
									res.json({ source: source, regions: regions });
								} else {
									Error.respond(res, err);
								}
							});
						}
					})
				)
			)
			.use('/resources', stonehenge.express.Router()
				.get('/', function (req, res) {
					Error.respond(res, new Error({
						message: 'Unsupported method',
						status: 400
					}));
				})
				.get('*', function (req, res) {
					var path = req.params[0];
					res.sendFile(req.stonehenge.scheme.name + '/resources' + path,
						{ root: __dirname + '/../../content/schemes/' },
						function (err) {
							if (err) {
								Error.respond(res, new Error({
									message: 'The resource does not exist',
									status: 404,
									resource: path,
									scheme: req.stonehenge.scheme.name
								}));
							}
						}
					);
				})
			)
		);
};