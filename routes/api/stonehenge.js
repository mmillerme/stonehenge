var Error = require('../../Error');
var path = require('path');

module.exports = function (stonehenge) {
	return stonehenge.express.Router()
		.get('/abstracts/:model', function (req, res) {
			res.sendFile(path.resolve(__dirname + '/../../models/' + req.params.model + '/$.js'), function (err) {
				if (err) {
					Error.respond(res, new Error(err));
				}
			});
		})
		.get('/models/:model', function (req, res) {
			res.sendFile(path.resolve(__dirname + '/../../models/' + req.params.model + '/client.js'), function (err) {
				if (err) {
					Error.respond(res, new Error(err));
				}
			});
		})
		.get('/config', function (req, res) {
			var config = require('../../config')();
			res.setHeader('Content-Type', 'text/javascript');
			res.send('define(function () { return ' + JSON.stringify(config) + '; });');
		})
		.use('/views', stonehenge.express.static('views'))
		.use(stonehenge.express.static('resources'));
};