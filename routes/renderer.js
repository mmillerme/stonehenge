var Error = require('../Error');
var Page = require('../models/Page/server');

module.exports = function (stonehenge) {
	return stonehenge.express.Router()
		.all('*', function (req, res) {
			var path = req.originalUrl;

			var lower = path.toLowerCase();
			if (path != lower) {
				res.redirect(301, lower);
				return;
			}

			var is_admin = false;
			if (path.substr(-1) == '!') {
				is_admin = true;
				path = path.substring(0, path.length - 1);
			}

			path.length > 1 && (path = path.substring(1));

			try {
				new Page(path, function (err) {
					if (!err) {
						var done = function (html) {
							res.send(html);
						};
						!is_admin ? this.render(done) : this.renderAdmin(done);
					} else {
						Error.respond(res, err);
					}
				});
			} catch (error) {
				Error.respond(res, error);
			}
		});
};